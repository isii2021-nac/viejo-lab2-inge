﻿using OldLab02.Domain.Core.Entities;
using OldLab02.Domain.Core.ValueObjects;
using OldLab02.Domain.HockeyLeague.ValueObjects;

namespace OldLab02.Domain.HockeyLeague.Entities
{
    public class Player : Entity
    {
        public RequiredString Name { get; }
        public Team? Team { get; private set; }
        public JerseyNumber JerseyNumber { get;  }

        public Player(RequiredString name, Team? team, JerseyNumber jerseyNumber)
        {
            Name = name;
            Team = team;
            JerseyNumber = jerseyNumber;
        }

        // for EFCore
        private Player(JerseyNumber jerseyNumber)
        {
            JerseyNumber = jerseyNumber;
            Team = null!;
            Name = null!;
        }

        public void AssignTeam(Team? team)
        {
            Team = team;
        }
    }
}
