﻿using System.Threading.Tasks;
using OldLab02.Domain.Core.Entities;
namespace OldLab02.Domain.Core.Repositories
{
    public interface IRepository<TAggregateRoot> where TAggregateRoot :
    AggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
