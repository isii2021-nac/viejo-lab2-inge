﻿using System.Collections.Generic;
using LanguageExt;

namespace OldLab02.Domain.Core.ValueObjects
{
    public class RequiredString : ValueObject
    {
        public const int MaxLength = 1000;
        public string Value { get; }
        private RequiredString(string value)
        {
            Value = value;
        }
        // for EFCore
        private RequiredString()
        {
            Value = null!;
        }
        public static Validation<ValidationError, RequiredString>
        TryCreate(string? value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return new IsNullOrWhitespace();
            if (value.Length > MaxLength)
                return new TooLong(MaxLength);
            return new RequiredString(value);
        }
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
        public override string ToString()
        {
            return Value;
        }

        public abstract record ValidationError;
        public record IsNullOrWhitespace : ValidationError;
        public record TooLong(int Maxlength) : ValidationError;
    }
}
